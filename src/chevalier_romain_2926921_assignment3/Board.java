import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Board extends Pane {

	// consts
	private final static int BLACK_PIECE = 2;
	private final static int WHITE_PIECE = -2;
	private final static int EMPTY = 1;
	private final static int VOID = 0;
	private static final int[][] BOARD = { { 2, 2, 2, 2, 2, 0, 0, 0, 0 }, { 2, 2, 2, 2, 2, 2, 0, 0, 0 },
			{ 1, 1, 2, 2, 2, 1, 1, 0, 0 }, { 1, 1, 1, 1, 1, 1, 1, 1, 0 }, { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 0 }, { 1, 1, -2, -2, -2, 1, 1, 0, 0 }, { -2, -2, -2, -2, -2, -2, 0, 0, 0 },
			{ -2, -2, -2, -2, -2, 0, 0, 0, 0 } };

	// vars
	private Piece[][] _boardRenders;
	private Rectangle _back;

	private int _current_player;
	private Boolean _piece_selected;
	private Piece _last_move;

	private GameLogic _logic;
	
	// constructors
	Board() {
		this.setBoard();
		this._boardRenders = this.generatePieces(BOARD);
		this.drawPiecesBoard(this._boardRenders);
		this.showPiecesInfos(this._boardRenders);
		this._current_player = BLACK_PIECE;
		this._piece_selected = false;
		this._logic = new GameLogic(this);
	}

	// private functions
	private void setBoard() {
		this._back = new Rectangle();
		this._back.setFill(Color.BLACK);
		this._back.setStroke(Color.RED);
		getChildren().add(this._back);
	}

	private Piece[][] generatePieces(int[][] board) {
		Piece[][] tmp = new Piece[board.length][board.length];
		for (int i = 0; i < board.length; ++i) {
			for (int j = 0; j < board[i].length; ++j) {
				if (board[i][j] == BLACK_PIECE)
					tmp[i][j] = new Piece(this, BLACK_PIECE, j, i);
				else if (board[i][j] == WHITE_PIECE)
					tmp[i][j] = new Piece(this, WHITE_PIECE, j, i);
				else if (board[i][j] == EMPTY)
					tmp[i][j] = new Piece(this, EMPTY, j, i);
				else if (board[i][j] == VOID)
					tmp[i][j] = new Piece(this, VOID, j, i);
			}
			System.out.print("\n");
		}
		return tmp;
	}

	private HBox drawPiecesLine(Piece[] line, int y) {
		HBox tmp = new HBox();
		for (int i = 0; i < line.length; ++i) {
			if (line[i]._type == BLACK_PIECE)
				tmp.getChildren().add(line[i]);
			else if (line[i]._type == WHITE_PIECE)
				tmp.getChildren().add(line[i]);
			else if (line[i]._type == EMPTY)
				tmp.getChildren().add(line[i]);
			else if (line[i]._type == VOID)
				;
		}
		tmp.setAlignment(Pos.CENTER);
		return tmp;
	}

	private void drawPiecesBoard(Piece[][] boardRenders) {
		GridPane gridPane = new GridPane();
		for (int i = 0; i < boardRenders.length; ++i) {
			gridPane.addRow(i, drawPiecesLine(boardRenders[i], i));
		}
		getChildren().add(gridPane);
	}

	private void showPiecesInfos(Piece[][] boardRenders) {
		for (int i = 0; i < boardRenders.length; ++i) {
			for (int j = 0; j < boardRenders[i].length; ++j) {
				System.out.print(boardRenders[i][j]._type + "=" + boardRenders[i][j]._x + boardRenders[i][j]._y + " ");
			}
			System.out.print("\n");
		}
	}

	// public functions
	public void refresh(Piece[][] boardRenders) {
		for (int i = 0; i < boardRenders.length; ++i) {
			for (int j = 0; j < boardRenders[i].length; ++j) {
				if (boardRenders[i][j]._type == BLACK_PIECE)
					boardRenders[i][j].setBodyColor(Color.BLUE);
				else if (boardRenders[i][j]._type == WHITE_PIECE)
					boardRenders[i][j].setBodyColor(Color.RED);
				else if (boardRenders[i][j]._type == EMPTY)
					boardRenders[i][j].setBodyColor(Color.WHITE);
			}
		}
	}

	public void changeCurrentPlayer() {
		if (this._current_player == BLACK_PIECE)
			this._current_player = WHITE_PIECE;
		else
			this._current_player = BLACK_PIECE;
	}

	public void swap(Piece p, Piece p2) {
		this._boardRenders[p2._y][p2._x]._type = this._boardRenders[p._y][p._x]._type;
		this._boardRenders[p._y][p._x]._type = EMPTY;
		this.refresh(this._boardRenders);
	}

	// getter
	public Piece[][] getBoardRenders() {
		return this._boardRenders;
	}

	public int getCurrentPlayer() {
		return this._current_player;
	}

	public Piece getLastMove() {
		return this._last_move;
	}

	public Boolean getPieceSelected() {
		return this._piece_selected;
	}

	public GameLogic getGameLogic() {
		return this._logic;
	}
	
	// setter
	public void setCurrentPlayer(int current_player) {
		this._current_player = current_player;
	}

	public void setLastMove(Piece last_move) {
		this._last_move = last_move;
	}

	public void setPieceSelected(Boolean piece_selected) {
		this._piece_selected = piece_selected;
	}
}
