import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {

	// vars
	private StackPane _stack_pane;
	private HBox _hb;
	private CustomControl _cc_custom;


	// constructors
	public Main() {

	}

	// public functions
	@Override
	public void init() {
		this._hb = new HBox();
		this._cc_custom = new CustomControl();
		this._hb.getChildren().add(this._cc_custom);
		_hb.setAlignment(Pos.CENTER);
	}

	@Override
	public void start(Stage primaryStage) {	
		primaryStage.setTitle("Custom Control Example");
		primaryStage.setScene(new Scene(this._hb, 550, 550));
		//primaryStage.setResizable(false);
		primaryStage.show();
	}

	@Override
	public void stop() {
	}

	public static void main(String[] args) {
		launch(args);
	}
}
