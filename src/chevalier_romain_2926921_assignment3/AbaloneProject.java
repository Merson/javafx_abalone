package chevalier_romain_2926921_assignment3;

import java.io.Console;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.effect.Blend;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Pair;

public class AbaloneProject extends Application {
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Abalone");
		
		LogicBoard = new GameLogic();
	}
	
	// overridden stop method
	public void stop() {
	}

	// entry point into our program to launch our JavaFX application
	public static void main(String[] args) {
		launch(args);
	}
	
	public GameLogic LogicBoard;

}
