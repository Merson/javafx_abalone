import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import javafx.util.Pair;

public class GameLogic {

	public GameLogic(Board board) {
		this._boardReference = board;
		this.board = this._boardReference.getBoardRenders();
		/*Push(5, 3, Dir.TOPRIGHT);

		for (int i = 0; i < 9; i++) {
			String str = "";
			if (i < 4)
				for (int k = 4 - i; k > 0; --k)
					str += " ";
			else if (i > 4)
				for (int k = i - 4; k > 0; --k)
					str += " ";
			for (int j = 0; j < 9; j++) {
				{
					if (board[i][j]._type == -2)
						str = str + " " + 3;
					else if (board[i][j]._type == 2)
						str = str + " " + "2";
					else if (board[i][j]._type == EMPTYCASE)
						str = str + " " + "-";
					else if (board[i][j]._type == 0)
						str = str + " " + "";
					else
						str = str + " " + board[i][j];
				}
			}
			System.out.println(str);
		}*/
	}

	/*
	public void Reset() {
		GenerateBoard();
		PlacePlayer();
	}

	
	void GenerateBoard() {
		int tabSize = 2 * boardSize - 1;

		board = new Piece[tabSize][tabSize];

		for (int i = 0; i < tabSize; i++) {
			for (int j = 0; j < 2 * boardSize - 1; j++) {
				board[i][j] = new Piece(EMPTYCASE, j, i);
			}
		}

		for (int i = boardSize - 1; i > 0; --i) {
			for (int j = 1; j < i + 1; j++) {
				board[(boardSize - 1) + i][tabSize - j]._type = VOID;
				board[(boardSize - 1) - i][tabSize - j]._type = VOID;
			}
		}
	}

	void PlacePlayer() {
		for (int i = 0; i < 9; i++) {
			if (board[0][i]._type == EMPTYCASE)
				board[0][i]._type = WHITEPIECE;
			if (board[1][i]._type == EMPTYCASE)
				board[1][i]._type = WHITEPIECE;
			if (board[7][i]._type == EMPTYCASE)
				board[7][i]._type = BLACKPIECE;
			if (board[8][i]._type == EMPTYCASE)
				board[8][i]._type = BLACKPIECE;
		}
		board[2][2]._type = WHITEPIECE;
		board[2][3]._type = WHITEPIECE;
		board[2][4]._type = WHITEPIECE;
		board[6][2]._type = BLACKPIECE;
		board[6][3]._type = BLACKPIECE;
		board[6][4]._type = BLACKPIECE;
	}
	*/

	public MyPair getNearCase(int x, int y, Dir d) {
		if (d == Dir.TOPLEFT) {
			if (y <= boardSize)
				return (new MyPair(x - 1, y - 1));
			else
				return (new MyPair(x, y - 1));
		} else if (d == Dir.TOPRIGHT) {
			if (y <= boardSize)
				return (new MyPair(x, y - 1));
			else
				return (new MyPair(x + 1, y - 1));
		} else if (d == Dir.RIGHT)
			return (new MyPair(x + 1, y));
		else if (d == Dir.BOTTRIGHT) {
			if (y >= boardSize)
				return (new MyPair(x, y + 1));
			else
				return (new MyPair(x + 1, y + 1));
		} else if (d == Dir.BOTTLEFT) {
			if (y >= boardSize)
				return (new MyPair(x - 1, y + 1));
			else
				return (new MyPair(x, y + 1));
		} else
			return (new MyPair(x - 1, y));
	}

	public Boolean Push(int x, int y, Dir d) {

		MyPair current = new MyPair(x, y);
		int MyColor = board[current.y][current.x]._type;
		List<MyPair> l = new ArrayList<MyPair>();

		if (MyColor == VOID || MyColor == EMPTYCASE)
			return (false);

		// Get All piece on the line
		while (current.x >= 0 && current.y >= 0 && current.x < (2 * boardSize) - 2 && current.y < (2 * boardSize) - 2
				&& board[current.y][current.x]._type != VOID) {
			l.add(current);
			current = getNearCase(current.x, current.y, d);
		}

		// Get nb of piece of the same color
		int nbOfPiece = 0;
		while (nbOfPiece < l.size() && board[l.get(nbOfPiece).y][l.get(nbOfPiece).x]._type == MyColor)
			++nbOfPiece;

		// Get piece after mine
		int PieceAfterMine = VOID;
		if (l.size() > nbOfPiece) {
			PieceAfterMine = board[l.get(nbOfPiece).y][l.get(nbOfPiece).x]._type;
		}

		// [...] // genre chopper le nb d'adv coll� dans la ligne

		if (nbOfPiece <= 3 && PieceAfterMine == EMPTYCASE) {
			board[l.get(0).y][l.get(0).x]._type = EMPTYCASE;
			for (int i = 0; i < nbOfPiece; i++)
				board[l.get(i + 1).y][l.get(i + 1).x]._type = MyColor;
		} else if (nbOfPiece <= 3 && PieceAfterMine == VOID) {
			board[l.get(0).y][l.get(0).x]._type = EMPTYCASE;
			for (int i = 0; i < nbOfPiece - 1; i++)
				board[l.get(i + 1).y][l.get(i + 1).x]._type = MyColor;
		} else if (nbOfPiece <= 3) {

			int nbOfAdv = 0;
			int cpt = nbOfPiece;
			while (cpt < l.size() && board[l.get(cpt).y][l.get(cpt).x]._type == -MyColor) {
				++cpt;
				++nbOfAdv;
			}

			if (nbOfPiece > nbOfAdv) {
				board[l.get(0).y][l.get(0).x]._type = EMPTYCASE;
				for (int i = 0; i < nbOfPiece; i++)
					board[l.get(i + 1).y][l.get(i + 1).x]._type = MyColor;
				for (int i = nbOfPiece; i < nbOfPiece + nbOfAdv - 1; i++)
					board[l.get(i + 1).y][l.get(i + 1).x]._type = -MyColor;
				if (nbOfAdv + nbOfPiece < l.size()) {
					board[l.get(nbOfAdv + nbOfPiece).y][l.get(nbOfAdv + nbOfPiece).x]._type = -MyColor;
				}
			} else
				return (false);
		}
		return (true);
	}

	public enum Dir {
		TOPLEFT, TOPRIGHT, RIGHT, BOTTRIGHT, BOTTLEFT, LEFT
	}

	public Piece[][] board;
	private final Board _boardReference;
	private final int boardSize = 5;
	private final int VOID = 0;
	private final int EMPTYCASE = 1;
	private final int BLACKPIECE = -2;
	private final int WHITEPIECE = 2;
}
