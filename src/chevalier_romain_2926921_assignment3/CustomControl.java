import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class CustomControl extends Control {

	//vars
	Board _board;
	
	//public functions
	public CustomControl() {
		setSkin(new CustomControlSkin(this));
		this._board = new Board();
		getChildren().add(this._board);	
		setOnKeyPressed(new EventHandler<KeyEvent>() {
			
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.SPACE)
					;
			}
		});
	}

	@Override
	public void resize(double width, double height) {
		super.resize(width, height);
	}
}
