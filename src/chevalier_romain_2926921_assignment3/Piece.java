import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.EllipseBuilder;
import javafx.scene.transform.Translate;

public class Piece extends Group {

	// consts
	private final static int BLACK_PIECE = 2;
	private final static int WHITE_PIECE = -2;
	private final static int EMPTY = 1;
	private final static int VOID = 0;

	// vars
	public int _type;
	public int _x;
	public int _y;
	private int _centerX;
	private int _centerY;
	private int _radiusX;
	private int _radiusY;
	public Board _board;

	private Color _color;
	private Ellipse _body;
	private Translate _pos;

	// constructors
	Piece(Board board, int type, int x, int y) {
		this._board = board;
		this._type = type;
		this._x = x;
		this._y = y;
		this._centerX = 0;
		this._centerY = 0;
		this._radiusX = 28;
		this._radiusY = 28;
		this.setPiece();
		//this.setControls(); //mes controls
	}

	Piece(Piece piece) {
		this(piece._board, piece._type, piece._x, piece._y);
		this._centerX = 0;
		this._centerY = 0;
		this._radiusX = 28;
		this._radiusY = 28;
		this.setPiece();
		this.setControls();
	}

	// private functions
	@SuppressWarnings("deprecation")
	private void setPiece() {
		this._body = EllipseBuilder.create().centerX(this._centerX).centerY(this._centerY).radiusX(this._radiusX)
				.radiusY(this._radiusY).build();
		this._pos = new Translate();
		if (this._type == BLACK_PIECE)
			this._color = Color.BLUE;
		else if (this._type == WHITE_PIECE)
			this._color = Color.RED;
		else if (this._type == EMPTY)
			this._color = Color.WHITE;
		else if (this._type == VOID)
			this._color = Color.GREY;
		else
			this._color = Color.YELLOW;
		this._body.setFill(this._color);
		this._body.setStroke(Color.BLACK);
		this._body.setStrokeWidth(3.0f);
		this._body.getTransforms().add(this._pos);
		getChildren().add(this._body);
	}

	private void setControls() {
		this.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				selectPiece();
				movePiece();
			}
		});
	}

	// public functions
	public void selectPiece() {

		// Selection of the piece
		if (this._type == EMPTY)
			if (!this._board.getPieceSelected())
				return;
		if (this._type == BLACK_PIECE || this._type == WHITE_PIECE) {
			if (this._board.getLastMove() != null)
				this._board.refresh(this._board.getBoardRenders());
			if (this._board.getCurrentPlayer() == this._type) {
				this._board.setLastMove(new Piece(this));
				if (this._board.getCurrentPlayer() == BLACK_PIECE)
					this.changeColor(Color.CADETBLUE);
				else
					this.changeColor(Color.DARKRED);
				this._board.setPieceSelected(true);
			}
		}
	}

	public void movePiece() {
		if (this._type == EMPTY) {
			if (!this._board.getPieceSelected())
				return;
			this._board.swap(this._board.getBoardRenders()[this._board.getLastMove()._y][this._board.getLastMove()._x],
					this);
			this._board.setPieceSelected(false);
			this._board.changeCurrentPlayer();
		}
	}

	public void changeColor(Color color) {
		this._color = color;
		this._body.setFill(color);
	}

	@Override
	public void resize(double width, double height) {
		super.resize(width, height);
	}

	@Override
	public void relocate(double x, double y) {
		super.relocate(x, y);
	}

	// getter
	public Color getColor() {
		return this._color;
	}

	// setter
	void setBodyColor(Color color) {
		this._body.setFill(color);
	}
}
